# From data_raw directory, merge all the csv files that ends with 0
# into one csv
import os

def readfile(file):
	with open(file, 'r', encoding='utf8') as f:
		return f.read()

# Combine CSVs
new_csv = ""
header = ""
for file in os.listdir('data_raw'):
	if file.endswith('.csv'):
		tmp = readfile('data_raw/' + file).split('\n')
		header = tmp[0]
		rows = tmp[1:]
		new_csv += '\n'.join(rows) + "\n"
new_csv = [i for i in new_csv.split('\n') if i]

# Remove Duplicates
new_csv_unique = list(set(new_csv))

# Add Header
new_csv = header + '\n' + '\n'.join(new_csv)
new_csv_unique = header + '\n' + '\n'.join(new_csv_unique)

# Save new_csv to a csv file
with open('data_merged/merged_playlist.csv', 'w+', encoding='utf8') as f:
	f.write(new_csv)
with open('data_merged/merged_playlist_unique.csv', 'w+', encoding='utf8') as f:
	f.write(new_csv_unique)