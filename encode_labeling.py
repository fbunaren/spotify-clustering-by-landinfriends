import pandas as pd


df = pd.read_csv(r'data_raw\cleandata.csv', index_col=0)
# get the genre column
genre = df['genre']
# get the unique genre
unique_genre = genre.unique()
print(unique_genre) # list the unique genres
# get the number of unique genres
num_unique_genre = len(unique_genre)
print(num_unique_genre) # print the number of unique genres


# one hot encoding for genres
df = pd.get_dummies(df, columns=['genre'])
# save dataframe
df.to_csv(r'one_hot_playlist.csv')






