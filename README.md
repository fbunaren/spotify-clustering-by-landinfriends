# Spotify Clustering by LandiNFriends

Spotify Clustering LandiNFriends Project

## About this Project
The project aims to develop a music recommendation system for Spotify users in order to provide a good music recommendation, so that the listening time on Spotify may increase and the users have better user experience when using the app.

## Code & Python Notebook
By default, our team writes the code as a Python file. 
However, to make it more convenient for others to see the code, we also create the Python Notebook.
The notebook can be accessed directly here:
- [Checkpoint 3 - Python Notebook](LandiNFriends_Checkpoint3.ipynb)

## Authors and acknowledgment
- Fransiscus Emmanuel Bunaren - 1806173506
- Fauzan Nazranda Rizqan - 2006488013
- Winaldo Amadea Hestu - 2006520001
- Firlandi Althaf Rizqi Ansyari - 2006489874