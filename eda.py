# Exploratory Data Analysis

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

attributes = "title,artist,genre,year,bpm,nrgy,dnce,dB,live,val,dur,acous,spch,popularity,has_win_award".split(",")
num_attr = "bpm,nrgy,dnce,dB,live,val,dur,acous,spch,popularity".split(",")
cat_attr = "genre,year,has_win_award".split(",")

df = pd.read_csv('data_merged/merged_playlist.csv')
df_unique = pd.read_csv('data_merged/merged_playlist_unique.csv')
df_num = df[num_attr]

# Create correlation matrix
corr = df_num.corr()
# Plot correlation matrix
sns.heatmap(corr, xticklabels=corr.columns, yticklabels=corr.columns, cmap='RdYlGn_r',annot=True)
plt.show()

# Plot scatterplot for db and nrgy
plt.scatter(df_num['dB'], df_num['nrgy'])
plt.show()

# Plot scatterplot for acous and nrgy
plt.scatter(df_num['acous'], df_num['nrgy'])
plt.show()

# Analyze Genre
df_genre = df['genre']
print("Number of genres: ", df_genre.nunique())
print("Number of missing values:",df_genre.isnull().sum())
top_10_genre = df_genre.value_counts().head(10)
print("Top 10 Genre")
print("==========================")
print(top_10_genre)
plt.show()

# Analyze Artist
df_artist = df_unique['artist']
print("Number of artist: ", df_artist.nunique())
print("Number of missing values:",df_artist.isnull().sum())
top_10_artist = df_artist.value_counts().head(10)
print("Top 10 Artist")
print("==========================")
print(top_10_artist)
print()

# Most popular genre for each year
genre_year = dict()
df_year_genre = df_unique[['year','genre']]
for year in df['year'].unique():
	# Get all genre with specific year and sort descending
	tmp_df_year = df_year_genre[df['year'] == year].value_counts().sort_values(ascending=False)
	genre_year[year] = tmp_df_year.index[0][1]
print("Most common genre (per year)")
print("==========================")
# Sort descending and print
for year in sorted(genre_year.keys(), reverse=True):
	print(year, genre_year[year])
print()

# Analyze correlation between duration and popularity
plt.scatter(df_num['dur'], df_num['popularity'])
plt.show()

# Create box plot for numeric attributes
# Also show the attribute name in the plot
# Use pandas
df_num.boxplot(column=num_attr)
plt.show()