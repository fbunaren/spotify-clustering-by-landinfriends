import pandas as pd #for data processing
import numpy as np #for numerical operations
import seaborn as sns #for statistical visualization
import plotly.express as px #for statistical visualization
import matplotlib.pyplot as plt #for visualization
import scipy.stats as scp #for mathematical stats

# 1950.csv
df = pd.read_csv(r'data_raw\1950.csv', index_col=0) #read in data
total_rows, total_attributes = df.shape #get number of rows and columns
print('Total Rows:', total_rows) #print total rows
print(df.describe()) #print descriptive statistics
df.info() #print data type and memory usage

# Then we apply the same process to the other data files

# 1960.csv
df = pd.read_csv(r'data_raw\1960.csv', index_col=0) 
total_rows, total_attributes = df.shape 
print('Total Rows:', total_rows) 
print(df.describe()) 
df.info() #

# 1970.csv
df = pd.read_csv(r'data_raw\1970.csv', index_col=0) 
total_rows, total_attributes = df.shape     
print('Total Rows:', total_rows) 
print(df.describe()) 
df.info() 

# 1980.csv
df = pd.read_csv(r'data_raw\1980.csv', index_col=0) 
total_rows, total_attributes = df.shape
print('Total Rows:', total_rows)
print(df.describe())
df.info()

# 1990.csv
df = pd.read_csv(r'data_raw\1990.csv', index_col=0)
total_rows, total_attributes = df.shape
print('Total Rows:', total_rows)
print(df.describe())
df.info()

# 2000.csv
df = pd.read_csv(r'data_raw\2000.csv', index_col=0)
total_rows, total_attributes = df.shape
print('Total Rows:', total_rows)
print(df.describe())
df.info()

# 2010.csv
df = pd.read_csv(r'data_raw\2010.csv', index_col=0)
total_rows, total_attributes = df.shape
print('Total Rows:', total_rows)
print(df.describe())
df.info()

# top10s.csv
df = pd.read_csv(r'data_raw\top10s.csv', index_col=0)
total_rows, total_attributes = df.shape
print('Total Rows:', total_rows)
print(df.describe())
df.info()

# Based on the above, we can see that the data that we are collecting satisfies the requirement that we need since we are dealing with a numerical attributes which fits perfectly with the clustering algorihm that we are going to implement later on.




